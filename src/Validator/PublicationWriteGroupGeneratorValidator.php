<?php

namespace App\Validator;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use ApiPlatform\Symfony\Validator\ValidationGroupsGeneratorInterface;
class PublicationWriteGroupGeneratorValidator implements ValidationGroupsGeneratorInterface
{

    public function __construct(
        private readonly Security $security
    )
    {
    }

    public function __invoke(object $object): array|GroupSequence
    {
        $user = $this->security->getUser();
        if ($user->getRoles() === ['ROLE_ADMIN']) {
            return ['publication:write:premium'];
        }
        return ['publication:write:normal'];
    }
}
