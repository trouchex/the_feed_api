<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\UtilisateurRepository;
use App\State\UtilisateurProcessor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: UtilisateurRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_LOGIN', fields: ['login'])]
#[UniqueEntity(fields: ['login'], message: 'Ce login est déjà utilisé.')]
#[UniqueEntity(fields: ['adresseEmail'], message: 'Cette adresse email est déjà utilisée.')]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new Post(
            validationContext: ['groups' => ['Default', 'utilisateur:create']],
            processor: UtilisateurProcessor::class),
        new Patch(
            denormalizationContext: ['groups' => ['utilisateur:update']],
            validationContext: ['groups' => ['Default', 'utilisateur:update']],
            processor: UtilisateurProcessor::class),
        new Delete()
    ],
    normalizationContext: ['groups' => ['utilisateur:read']],
)]
class Utilisateur implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['publication:read', 'utilisateur:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(min: 4, max: 20, minMessage: 'Le login doit contenir au moins {{ limit }} caractères', maxMessage: 'Le login doit contenir au maximum {{ limit }} caractères')]
    #[Groups(['publication:read', 'utilisateur:read'])]
    private ?string $login = null;

    #[ORM\Column]

    private array $roles = [];

    #[ORM\Column]
    #[ApiProperty(readable: false, writable: false)]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Email(message: 'L\'adresse email {{ value }} n\'est pas une adresse email valide.')]
    #[Groups(['publication:read', 'utilisateur:read'])]
    private ?string $adresseEmail = null;


    #[ORM\Column(options: ["default" => false])]
    #[ApiProperty(readable: true, writable: false)]
    #[Groups(['publication:read', 'utilisateur:read'])]
    private ?bool $premium = false;

    /**
     * @var Collection<int, PublicationApiResource>
     */
    #[ORM\OneToMany(targetEntity: PublicationApiResource::class, mappedBy: 'auteur', orphanRemoval: true)]
    private Collection $publicationApiResources;


    #[Assert\Length(min: 8, max: 255, minMessage: 'Le mot de passe doit contenir au moins {{ limit }} caractères', maxMessage: 'Le mot de passe doit contenir au maximum {{ limit }} caractères')]
    #[Assert\Regex(pattern: '#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{8,255}$#', message: 'Le mot de passe doit contenir au moins une minuscule, une majuscule et un chiffre')]
    #[Assert\NotNull(groups: ['utilisateur:create'])]
    #[Assert\NotBlank(groups: ['utilisateur:create'])]
// N’oubliez pas d’importer les classes correspondantes.
// Aussi, en utilisant un autre attribut (provenant d’API Platform),
// faites en sorte que cette propriété ne puisse jamais pouvoir être lue
// par les utilisateurs (jamais affichée/normalisée quand on renvoi une ressource type utilisateur).
    #[ApiProperty(readable: false)]
    private ?string $plainPassword = null;

    public function __construct()
    {
        $this->publicationApiResources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): static
    {
        $this->login = $login;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->login;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function getAdresseEmail(): ?string
    {
        return $this->adresseEmail;
    }

    public function setAdresseEmail(string $adresseEmail): static
    {
        $this->adresseEmail = $adresseEmail;

        return $this;
    }

    public function addRole($role) : void {
        if(!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
    }

    public function isPremium(): ?bool
    {
        return $this->premium;
    }

    public function setPremium(bool $premium): static
    {
        $this->premium = $premium;

        return $this;
    }

    /**
     * @return Collection<int, PublicationApiResource>
     */
    public function getPublicationApiResources(): Collection
    {
        return $this->publicationApiResources;
    }

    public function addPublicationApiResource(PublicationApiResource $publicationApiResource): static
    {
        if (!$this->publicationApiResources->contains($publicationApiResource)) {
            $this->publicationApiResources->add($publicationApiResource);
            $publicationApiResource->setAuteur($this);
        }

        return $this;
    }

    public function removePublicationApiResource(PublicationApiResource $publicationApiResource): static
    {
        if ($this->publicationApiResources->removeElement($publicationApiResource)) {
            // set the owning side to null (unless already changed)
            if ($publicationApiResource->getAuteur() === $this) {
                $publicationApiResource->setAuteur(null);
            }
        }

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): static
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
}