<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use App\Repository\PublicationApiResourceRepository;
use App\State\PublicationProcessor;
use App\Validator\PublicationWriteGroupGenerator;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PublicationApiResourceRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new GetCollection(
            uriTemplate: '/utilisateurs/{id}/publications',
            uriVariables: [
                'id' => new Link(fromProperty: 'publicationApiResources', fromClass: Utilisateur::class)
            ],
            normalizationContext: ['groups' => ['publication:read']]
        ),
        new Get(),
        new Post(denormalizationContext: ["publicationApiResource:create"], security: "is_granted('ROLE_USER')", validationContext: ["groups" => PublicationWriteGroupGenerator::class], processor: PublicationProcessor::class),
        new Delete(security: "is_granted('ROLE_USER') and object.getAuteur() == user")
    ],
    normalizationContext: ['groups' => ['publication:read']],
    order: ['datePublication' => 'ASC'],
)]
#[ORM\HasLifecycleCallbacks]
class PublicationApiResource
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['publication:read'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 4,
        max: 50,
        minMessage: "Le message est trop court! (4 caractères minimum)",
        maxMessage: "Le message est trop long! (50 caractères maximum)"
    )]
    #[Assert\Length(max: 50, maxMessage: 'Il faut moins de 50 caractères', groups: ['publication:write:normal'])]
    #[Assert\Length(max: 200, maxMessage: 'Il faut moins de 200 caractères', groups: ['publication:write:premium'])]
    #[Groups(['publication:read'])]
    private ?string $message = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[ApiProperty(writable: false)]
    #[Groups(['publication:read'])]
    private ?\DateTimeInterface $datePublication = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'publicationApiResources')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[ApiProperty(writable: false)]
    #[Groups(['publication:read'])]
    private ?Utilisateur $auteur = null;


    #[ORM\PrePersist]
    public function prePersistDatePublication() : void {
        $this->datePublication = new \DateTime();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->datePublication;
    }

    public function setDatePublication(\DateTimeInterface $datePublication): static
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    public function getAuteur(): ?Utilisateur
    {
        return $this->auteur;
    }

    public function setAuteur(?Utilisateur $auteur): static
    {
        $this->auteur = $auteur;

        return $this;
    }
}
