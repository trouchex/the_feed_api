<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UtilisateurProcessor implements ProcessorInterface
{

    public function __construct(
        #[Autowire(service: 'api_platform.doctrine.orm.state.persist_processor')]
        private readonly ProcessorInterface $persistProcessor,
        private readonly UserPasswordHasherInterface $passwordHasher
    )
    {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): mixed
    {
//        mdp null
        if ($data->getPlainPassword() === null) {
            return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
        }
        $data->setPassword($this->passwordHasher->hashPassword($data, $data->getPlainPassword()));
        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
