<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241010152107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE publication_api_resource ADD auteur_id INT NOT NULL');
        $this->addSql('ALTER TABLE publication_api_resource ADD CONSTRAINT FK_2E79275C60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2E79275C60BB6FE6 ON publication_api_resource (auteur_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE publication_api_resource DROP FOREIGN KEY FK_2E79275C60BB6FE6');
        $this->addSql('DROP INDEX IDX_2E79275C60BB6FE6 ON publication_api_resource');
        $this->addSql('ALTER TABLE publication_api_resource DROP auteur_id');
    }
}
